package controllers

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/superordinate/kDaemon/database"
	"github.com/superordinate/kDaemon/models"
	"gopkg.in/unrolled/render.v1"
	"net/http"
	"strconv"
)

type NodeController struct {
	AppController
	*render.Render
}

func (c *NodeController) CreateNode(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {

	//creates a new node object populated with JSON from data
	newnode := models.Node{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newnode)
	if err != nil {
		panic(err)
		return
	}

	//Validates the Node passed in

	if newnode.Validate() {
		//Adds the node to the database
		success, _ := database.CreateNode(&newnode)

		if success == false {
			c.JSON(rw, http.StatusConflict, "Node conflicts with existing node. Make sure your node is unique.")
			return
		}
		//return success message with new node information
		c.JSON(rw, http.StatusCreated, newnode)
	} else {

		body, _ := newnode.GetJSON()
		c.JSON(rw, http.StatusBadRequest, "Invalid format"+body)
	}

}

func (c *NodeController) DeleteNode(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	//Gets the node id
	nodeid, err := strconv.Atoi(p.ByName("id"))

	if err != nil {
		c.JSON(rw, http.StatusBadRequest, "invalid id")
		return
	}

	//Attempts to remove the node
	success, _ := database.DeleteNode(int64(nodeid))

	if !success {
		c.JSON(rw, http.StatusNotFound, "Node doesn't exist")
		return
	}

	c.JSON(rw, http.StatusOK, "Node deleted successfully")

}

func (c *NodeController) EditNode(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	//creates a new node object populated with JSON from data
	newnode := models.Node{}
	decoder := json.NewDecoder(r.Body)

	err := decoder.Decode(&newnode)
	if err != nil {
		panic(err)
		return
	}

	//Validates the Node passed in

	if newnode.Validate() {
		//Adds the node to the database
		success, _ := database.UpdateNode(&newnode)

		if success == false {
			c.JSON(rw, http.StatusNotFound, "Node doesn't exist")
			return
		}
		//return success message with new node information
		c.JSON(rw, http.StatusCreated, newnode)
	} else {
		c.JSON(rw, http.StatusBadRequest, "Invalid format")
	}
}

func (c *NodeController) NodeInformation(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {

	//Gets the node id
	nodeid, err := strconv.Atoi(p.ByName("id"))

	if err != nil {
		c.JSON(rw, http.StatusBadRequest, "invalid id")
		return
	}

	//Attempts to retrieve the node from the database
	node, err := database.GetNode(int64(nodeid))

	if err != nil {
		c.JSON(rw, http.StatusNotFound, "Node doesn't exist")
		return
	}

	c.JSON(rw, http.StatusOK, node)
}

func (c *NodeController) AllNodes(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {

	//Attempts to retrieve the node from the database
	nodes, err := database.GetNodes()

	if err != nil {
		c.JSON(rw, http.StatusNotFound, "No nodes")
		return
	}

	c.JSON(rw, http.StatusOK, nodes)
}
